var gulp = require('gulp'), // Gulp JS
	sass = require('gulp-sass'), // Плагин для Sass
	//bump = require('gulp-bump'), // Обновление версий пакетов проекта
	csscomb = require('gulp-csscomb'), // Форматирование css
	minifyCss = require('gulp-minify-css'), // Минификация CSS
	autoprefixer = require('gulp-autoprefixer'), // Подстановка префиксов
	cssPrefix = require('gulp-css-prefix'),
	prettify = require('gulp-prettify'), // Форматирование html
	htmlhint = require('gulp-htmlhint'),
	jade = require('gulp-jade'), // Препроцессор jade -> html
	uglify = require('gulp-uglify'), // Минификация js
	plumber = require('gulp-plumber'),
	gulpif = require('gulp-if'),
	gutil = require('gulp-util'),
	concat = require('gulp-concat'), // Конкатенация файлов
	imagemin = require('gulp-imagemin'), // Минификация изображений
	spritesmith = require('gulp.spritesmith'), // Генератор спрайтов и CSS переменных
	browserSync = require('browser-sync'), // Локальный сервер и автоперезагрузка страницы
	dom = require('gulp-dom'), // Работа с DOM
	filter = require('gulp-filter'), // Фильтрация файлов
	reload = browserSync.reload,
	pathD = require('./paths'),

	path = require('path'),
	tap = require('gulp-tap'),
	merge = require('merge-stream'),
	watch = require('gulp-watch'),
	using = require('gulp-using'),
	// newer = require('gulp-newer'),
	// changed = require('gulp-changed'),
	// debug = require('gulp-debug'),
	// es = require('event-stream'),
	// log = require('gulp-util').log,
	pkg = require('./package.json');

var _globPath = {
	base: 'www/',
	ignore: '!**/libs/**/'
};
var _pathDir = {
	html: '*.html',

	js: '**/*.js',
	jsIgnoreMinDir: '!**/min/*.js',
	jsIgnoreMin: '!**/*.min.js',
	jsIgnore: _globPath.ignore + '*.js',

	jade: '**/*.jade',
	jadeAdd: '**/.default/*.jade',
	jadeCmp: 'bitrix/**/components/**/*.jade',
	jadeIgnore: '!**/_*.jade',

	scss: '**/*.scss',
	scssAdd: '**/.default/**/*.scss',
	scssIgnore: '!**/_*.scss',
	scssIgnoreLib: _globPath.ignore + '*.scss',

	sprite: '**/images/sprite/',
	spriteIgnore: _globPath.ignore + 'images/sprite/',
}
var _watchDir = {
	html: _globPath.base + _pathDir.html,
	js: _globPath.base + _pathDir.js,
	jade: _globPath.base + _pathDir.jade,
	jadeAdd: _globPath.base + _pathDir.jadeAdd,
	scss: _globPath.base + _pathDir.scss,
	scssIgnore: '!'+_globPath.base+'**/_*.scss',
	sprite: _globPath.base + _pathDir.sprite + '*.*',
};

// Вспомогательные функции для работы с DOM

function remove(node) {
	var parent = node.parentNode;
	parent.removeChild(node);
}
function setAttributes(el, attrs) {
	for(var key in attrs) {
		el.setAttribute(key, attrs[key]);
	}
}
function logFile(es) {
	return es.map(function(file, cb) {
		log(file.path);
		return cb();
	});
};


// Static Server + watching scss/html files
gulp.task('server', [
	'sass',
	'sprite',
	'compress',
	'jadeTemplate',
], function () {
	browserSync({
		server: {
			baseDir: _globPath.base
		},
		//		open: false
	});
	gulp.watch(_watchDir.scss, ['sass']);
	gulp.watch(_watchDir.sprite, ['sprite']);
	gulp.watch(_watchDir.js, ['compress']);
	gulp.watch([_watchDir.jade, _watchDir.jadeAdd], ['jadeTemplate']);
	gulp.watch(_watchDir.html).on('change', reload);
});

// Компиляция jade
gulp.task('jadeTemplate', function() {


	return gulp.src([
		_pathDir.jade, 
		_pathDir.jadeAdd, 
		_pathDir.jadeCmp, 
		_pathDir.jadeIgnore
	], {
		cwd: _globPath.base,
		base: './'
	})

		.pipe(tap(function (file,t) {
		var currentDir = path.dirname(file.path);
		var cmpFilter = filter([path.basename(file.path).indexOf('index') ? '!*' : '*'], {restore: true});

		gulp.src(file.path)
			.pipe(using({color: 'yellow'}))
			.pipe(plumber())
			.pipe(jade({
			pretty: true,
			data: {
				// tplPath: currentDir.match(/(bitrix\\.*dvt.+?\\)/g)
				// cmpPath: tplPath+'/components'
			}
		}))

			.pipe(cmpFilter)

			.pipe(dom(function(){
			var links = this.querySelectorAll('link[rel="stylesheet"]'),
				i = links.length;

			var hrefs = new Array();

			for(var j = 0; j < i; j++) {
				hrefs.push(links[j].getAttribute('href')); // Сохраняем href стилей
				remove(links[j]); // удаляем стили

				var style = this.createElement('link'); // Создаем новые линки на стили
				setAttributes(style, {
					'rel': 'stylesheet',
					'href': hrefs[j]
				})

				this.head.appendChild(style); // Добавляем в head
			}

			return this;
		}))

			.pipe(cmpFilter.restore)

			.pipe(htmlhint({
			"tag-pair": true,
			"style-disabled": true,
			"img-alt-require": true,
			"tagname-lowercase": true,
			"src-not-empty": true,
			"id-unique": true,
			"spec-char-escape": true
		}))
			.pipe(prettify({
			brace_style: 'expand',
			indent_size: 1,
			indent_char: '\t',
			indent_inner_html: true,
			preserve_newlines: true
		}))
			.pipe(gulp.dest(currentDir))
	}))

	// 		.pipe(plumber())
	// 		.pipe(jade({
	// 			pretty: true,
	// 		}))
	// 		.pipe(cmpFilter)
	// 		.pipe(dom(function(){
	// 			var links = this.querySelectorAll('link[rel="stylesheet"]'),
	// 				i = links.length;

	// 			var hrefs = new Array();

	// 			for(var j = 0; j < i; j++) {
	// 				hrefs.push(links[j].getAttribute('href')); // Сохраняем href стилей
	// 				remove(links[j]); // удаляем стили

	// 				var style = this.createElement('link'); // Создаем новые линки на стили
	// 				setAttributes(style, {
	// 					'rel': 'stylesheet',
	// 					'href': hrefs[j]
	// 				})

	// 				this.head.appendChild(style); // Добавляем в head
	// 			}

	// 			return this;
	// 		}))
	// //		.pipe(cmpFilter.restore)
	// 		.pipe(htmlhint({
	// 			"tag-pair": true,
	// 			"style-disabled": true,
	// 			"img-alt-require": true,
	// 			"tagname-lowercase": true,
	// 			"src-not-empty": true,
	// 			"id-unique": true,
	// 			"spec-char-escape": true
	// 		}))
	// 		.pipe(prettify({
	// 			brace_style: 'expand',
	// 			indent_size: 1,
	// 			indent_char: '\t',
	// 			indent_inner_html: true,
	// 			preserve_newlines: true
	// 		}))
	// 		.pipe(gulp.dest('.'))
});


// Компиляция sass фалов
gulp.task('sass', function () {
	// Путь к sass файлам
	return gulp.src([
		_pathDir.scss, 
		_pathDir.scssAdd, 
		_pathDir.scssIgnore,
		_pathDir.scssIgnoreLib
	], {
		cwd: _globPath.base,
		base: './'
	})
		.pipe(tap(function (file,t) {
		var currentDir = path.dirname(file.path);
		// console.log(file.path)

		var stylesData = gulp.src(file.path)
		.pipe(watch(file.path))
		.pipe(plumber())
		// Вывод ошибки компиляции
		.pipe(sass({
			errLogToConsole: true
		}))
		// Генерация префиксов
		.pipe(autoprefixer({
			browsers: [
				'Android >= ' + pkg.browsers.android,
				'Chrome >= ' + pkg.browsers.chrome,
				'Firefox >= ' + pkg.browsers.firefox,
				'Explorer >= ' + pkg.browsers.ie,
				'iOS >= ' + pkg.browsers.ios,
				'Opera >= ' + pkg.browsers.opera,
				'Safari >= ' + pkg.browsers.safari
			],
			cascade: false
		}))
		//.pipe(cssPrefix('dvt-'))

		// Минификация стилей
		.pipe(gulpif(!gutil.env.debug, minifyCss()))
		.pipe(gulpif(gutil.env.csscomb, csscomb()))
		.pipe(reload({
			stream: true
		}));

		// Путь скомпилированного css файла
		var scssStream = stylesData.pipe(gulp.dest(function(file){ 
			if(currentDir.indexOf('scss') > -1){
				return path.join(currentDir, '../')
			}
			else{
				return path.join(currentDir)
			}
		}))

		return scssStream;
	}))

	// 	.pipe(plumber())
	// 	// Вывод ошибки компиляции
	// 	.pipe(sass({
	// 		errLogToConsole: true
	// 	}))
	// 	// Генерация префиксов
	// 	.pipe(autoprefixer({
	// 		browsers: [
	// 		'Android >= ' + pkg.browsers.android,
	// 		'Chrome >= ' + pkg.browsers.chrome,
	// 		'Firefox >= ' + pkg.browsers.firefox,
	// 		'Explorer >= ' + pkg.browsers.ie,
	// 		'iOS >= ' + pkg.browsers.ios,
	// 		'Opera >= ' + pkg.browsers.opera,
	// 		'Safari >= ' + pkg.browsers.safari
	// 	],
	// 		cascade: false
	// 	}))
	// //.pipe(cssPrefix('dvt-'))

	// // Минификация стилей
	// .pipe(gulpif(!gutil.env.debug, minifyCss()))
	// 	.pipe(gulpif(gutil.env.csscomb, csscomb()))

	// // Путь скомпилированного css файла
	// 	.pipe(gulp.dest('.'))
	// 	.pipe(reload({
	// 		stream: true
	// 	}));
})

// Генерация спрайтов
gulp.task('sprite', function () {
	return gulp.src([
		_pathDir.sprite, 
		_pathDir.spriteIgnore
	])
		.pipe(tap(function (file,t) {
		var currentDir = path.dirname(file.path);
		// console.log('Current Dirname = '+currentDir);

		var spriteData = gulp.src(file.path+'/*.*')
		.pipe(plumber())
		.pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '_sprite.scss',
			cssFormat: 'scss',
			algorithm: 'binary-tree',
			padding: 5,
			//cssTemplate: 'sass.template.mustache',
			cssVarMap: function (sprite) {
				sprite.name = 'ico-' + sprite.name
			}
		}));

		// Pipe Image stream
		var imgStream = spriteData.img
		.pipe(imagemin()) // Оптимизируем изображение
		.pipe(gulp.dest(currentDir)); // Путь куда сохраняем картинки

		// Pipe CSS stream
		var cssStream = spriteData.css
		.pipe(gulp.dest( function(file){ // Путь куда сохраняем mixin _sprite.scss
			// if(currentDir.indexOf('components') > -1){
			// 	return path.join(currentDir, '../') // Путь mixin'a если спрайт в папке компонента
			// }
			// else{
			return path.join(currentDir, '../scss/') // Путь mixin'a если спрайт в папке шаблона
			// }
		}));

		// Return a merged stream to handle both `end` events
		return merge(imgStream, cssStream);

	}))
});

// Минификация js
gulp.task('compress', function () {
	return gulp.src([
		_pathDir.js, 
		_pathDir.jsIgnoreMinDir, 
		_pathDir.jsIgnoreMin, 
		_pathDir.jsIgnore
	], {
		cwd: _globPath.base,
		base: './'
	})
		.pipe(plumber())
		.pipe(concat('common.min.js'))
		.pipe(gulpif(!gutil.env.debug, uglify()))
		.pipe(gulp.dest('.'))
});


gulp.task('default', ['server']);